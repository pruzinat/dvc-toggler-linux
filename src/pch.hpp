#ifndef PCH_HPP
#define PCH_HPP

#ifdef __cplusplus

#include <algorithm>
#include <chrono>
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <inttypes.h>
#include <iostream>
#include <mutex>
#include <QCryptographicHash>
#include <QDialog>
#include <QObject>
#include <QPushButton>
#include <QSettings>
#include <QSharedMemory>
#include <QStandardPaths>
#include <QSystemSemaphore>
#include <QSystemTrayIcon>
#include <QTabWidget>
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtDebug>
#include <QWidget>
#include <shared_mutex>
#include <sstream>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <thread>
#include <time.h>
#include <unordered_set>
#include <vector>

#endif // __plusplus

#endif // PCH_HPP
